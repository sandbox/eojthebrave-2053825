By itself the Entity Admin UI module doesn't do much, but combined with the
Entity API module it can make creating an admin listing interface for your
custom entities a breeze.

The Entity API provides a very basic and somewhat limited main administration
listing page with just a table and a pager. This module provides an extension
of the EntityDefaultUIController class with a more full featured listing page
and a few other bells and whistles that makes it really easy to add the ability
to filter and sort on your admin pages. Think admin/content or admin/user pages
in Drupal core and the handy filtering options they provide.

Usage:
--------------------------------------------------------------------------------
Simply use the EntityAdminUIDefaultUIController class in place of
EntityDefaultUIController to start taking advantage of the features it provides.

The most common use case where will be in hook_entity_info().

Example:
--------------------------------------------------------------------------------
function hook_entity_info() {
  $info['my_entity'] = array(
    ...
    'admin ui' => array(
      'path' => 'admin/my_entity',
      'controller class' => 'EntityAdminUIDefaultUIController',
      'menu wildcard' => '%my_entity',
    ),
    ...
  );
  return $info;
}

To build off the foundation provided simpley extend the
EntityAdminUIDefaultUIController class with your own class and override the
public methods.

The most common use case here is to extend the set of filters used and/or the
properties listed on the overview listing page.

You can extend the set of filters that are available and the data columns
used in the listing table by overriding the overviewPropertyList() method and
returning an array of information about the properties to use.

The array is an associative array where the key is the name of the property and
the value is an array with with following keys.

- 'title': Human readable label for this filter.
- 'options': An array of options suitable for use in the #options of a select
  element.

eg.

$properties['status'] = array(
  'title' => t('Status'),
  'options' => array('[any]' => t('Any'), 0 => 'Unpublished', 1 => 'Published'),
);

Or, if you just want to to list the property but not allow people to filter
based on it set the value to NULL.

eg.

$properties['title'] = NULL;

A slightly more complicated & complete example.

public function overviewPropertyList() {
  $types = array('[any]' => t('any'));
  foreach (membership_entity_get_types() as $type) {
    $types[$type->type] = $type->name;
  }

  return array(
    'member_id' => NULL,
    'status' => array(
      'title' => t('Status'),
      'options' => array(
        '[any]' => t('any'),
      ) + membership_entity_get_status_list(),
    ),
    'type' => array(
      'title' => t('Type'),
      'options' => $types,
    ),
    'created' => NULL,
    'changed' => NULL,
  );
}

Help:
--------------------------------------------------------------------------------
Please post all questions in the issue queue for this module after searching
for duplicate questions.

Credits:
--------------------------------------------------------------------------------
Joe Shindelar (eojthebrave)
