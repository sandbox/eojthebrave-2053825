<?php
/**
 * @file
 * Define an enhanced UI Controller for entities.
 */

class EntityAdminUIDefaultUIController extends EntityDefaultUIController {
  protected $propertyInfo;

  public function __construct($entity_type, $entity_info) {
    parent::__construct($entity_type, $entity_info);
    $this->propertyInfo = entity_get_all_property_info($this->entityType);
  }

  public function overviewPropertyList() {
    $info = array();
    // Add the label field.
    $info[$this->entityInfo['entity keys']['label']] = NULL;
    // Add the ID field.
    $info[$this->entityInfo['entity keys']['id']] = NULL;

    if (isset($this->entityInfo['bundle keys']['bundle']) && isset($this->entityInfo['bundles'])) {
      $bundles = array();
      foreach ($this->entityInfo['bundles'] as $key => $value) {
        $bundles[$key] = $value['label'];
      }
      $info[$this->entityInfo['entity keys']['bundle']] = array(
        'title' => t('Bundle'),
        'options' => array(
          '[any]' => t('any'),
        ) + $bundles,
      );
    }
    return $info;
  }

  /**
   * Get a list of filters that can be used to narrow the overview list.
   *
   * Currently this will only work with properties and not fields.
   *
   * @return array
   *   An array of filter options keyed by property name with the following
   *   sub array.
   *    'title': Label to be used on the form field.
   *    'options': An array of options for use with a #type = select list.
   */
  public function overviewFilters() {
    $filters = $this->overviewPropertyList();
    $filters = array_filter($filters, 'is_array');
    return $filters;
  }

  /**
   * Builds the entity overview page.
   */
  public function overviewForm($form, &$form_state) {
    if (isset($form_state['values']['operation']) && $form_state['values']['operation'] != '[choose]' ) {
      return $this->bulkOperationsConfirm($form, $form_state);
    }
    // Add any filters.
    if ($filters = $this->overviewFilters()) {
      $form['filters'] = $this->buildFiltersForm($filters);
    }

    // Add the bulk operations form.
    $form['operations'] = $this->bulkOperationsForm($form, $form_state);

    // Add the main listing of entities.
    $form['table'] = $this->overviewTable();
    $form['pager'] = array('#theme' => 'pager');
    return $form;
  }

  /**
   * Label to display above filters form on overview page.
   *
   * @return string
   */
  public function filtersLabel() {
    return t('Show only @type where', array('@type' => $this->entityInfo['label']));
  }

  /**
   * Create the form used for applying a set of filters to the overview list.
   */
  public function buildFiltersForm($filters) {
    $session = isset($_SESSION[$this->entityType . '_overview_filter']) ? $_SESSION[$this->entityType . '_overview_filter'] : array();

    $form = array(
      '#type' => 'fieldset',
      '#title' => $this->filtersLabel(),
      '#theme' => 'exposed_filters__entityui',
    );

    $i = 0;
    foreach ($session as $filter) {
      list($type, $value) = $filter;
      $value = $filters[$type]['options'][$value];

      $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
      if ($i++) {
        $form['current'][] = array('#markup' => t('and where %property is %value', $t_args));
      }
      else {
        $form['current'][] = array('#markup' => t('where %property is %value', $t_args));
      }

      if (in_array($type, array('type', 'status'))) {
        // Remove the option if it is already being filtered on.
        unset($filters[$type]);
      }
    }

    $form['status'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('clearfix')),
      '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
    );
    $form['status']['filters'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('filters')),
    );
    foreach ($filters as $key => $filter) {
      $form['status']['filters'][$key] = array(
        '#type' => 'select',
        '#options' => $filter['options'],
        '#title' => $filter['title'],
        '#default_value' => '[any]',
      );
    }

    $form['status']['actions'] = array(
      '#type' => 'actions',
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['status']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => count($session) ? t('Refine') : t('Filter'),
      '#submit' => array('entityadminui_filter_form_submit'),
    );
    if (count($session)) {
      $form['status']['actions']['undo'] = array(
        '#type' => 'submit',
        '#value' => t('Undo'),
        '#submit' => array('entityamdinui_filter_form_submit'),
      );
      $form['status']['actions']['reset'] = array(
        '#type' => 'submit',
        '#value' => t('Reset'),
        '#submit' => array('entityadminui_filter_form_submit'),
      );
    }

    drupal_add_js('misc/form.js');

    return $form;
  }

  /**
   * Handle submission of the filters form.
   *
   * Save submitted data into a session for use later in filtering.
   */
  public function filtersFormSubmit($form, &$form_state) {
    $filters = $this->overviewFilters();
    switch ($form_state['values']['op']) {
      case t('Filter'):
      case t('Refine'):
        // Apply every filter that has a choice selected other than 'any'.
        foreach ($filters as $filter => $options) {
          if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
            // Flatten the options array to accommodate hierarchical/nested options.
            $flat_options = form_options_flatten($filters[$filter]['options']);
            // Only accept valid selections offered on the dropdown, block bad input.
            if (isset($flat_options[$form_state['values'][$filter]])) {
              $_SESSION[$this->entityType . '_overview_filter'][] = array($filter, $form_state['values'][$filter]);
            }
          }
        }
        break;

      case t('Undo'):
        array_pop($_SESSION[$this->entityType . '_overview_filter']);
        break;

      case t('Reset'):
        $_SESSION[$this->entityType . '_overview_filter'] = array();
        break;
    }
  }

  /**
   * Operations that can be performed on one or more selected entity.
   *
   * And array of operations where the key is the unique id of the operation
   * and the value contains;
   * - label: A human readable label for the operation
   * - callback: A callback function to perform the operation. Callback
   *   functions will receive the $form, $form_state, and an array of selected
   *   entity $ids on which the operation should be performed.
   *
   * @return array
   *   An associative array that describes the operations that can be performed
   *   on a selected item.
   */
  public function getOperations() {
    $options = array(
      'delete' => array(
        'label' => t('Delete'),
        'callback' => 'entityadminui_delete_operation',
      ),
    );

    drupal_alter($this->entityType . '_admin_operations', $options);
    return $options;
  }

  /**
   * Create the bulk operations form.
   */
  public function bulkOperationsForm($form, &$form_state) {
    $element = array();
    // Build the 'Update options' form.
    $element['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#attributes' => array('class' => array('container-inline')),
    );

    $options['[choose]'] = t('Choose ...');
    foreach ($this->getOperations() as $operation => $array) {
      $options[$operation] = $array['label'];
    }
    $element['options']['operation'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#title_display' => 'invisible',
      '#options' => $options,
    );
    $element['options']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#validate' => array('entityadminui_bulk_operations_validate'),
      '#submit' => array('entityadminui_bulk_operations_confirm'),
    );
    return $element;

  }

  /**
   * Validation handler for the bulk operations form.
   */
  public function bulkOperationsFormValidate($form, &$form_state) {
    // Make sure the operation chosen is valid.
    $operations = $this->getOperations();
    if (!isset($operations[$form_state['values']['operation']])) {
      form_set_error('operation', t('Invalid operation selected.'));
    }
    // Make sure there are items selected.
    if (!is_array($form_state['values']['table']) || !count(array_filter($form_state['values']['table']))) {
      form_set_error('', t('No items selected.'));
    }
  }

  /**
   * Confirmation form for performing a bulk operation on a set of entities.
   */
  public function bulkOperationsConfirm($form, &$form_state) {
    $operations = $this->getOperations();
    $operation = $operations[$form_state['values']['operation']];
    $ids = array_filter($form_state['values']['table']);
    $entities = entity_get_controller($this->entityType)->load($ids);
    //$label = entity_label($this->entityType, $entity);
    //$confirm_question = t('Are you sure you want to delete the %entity %label?', array('%entity' => $this->entityInfo['label'], '%label' => $label));
    $form['ids'] = array(
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
      '#tree' => TRUE,
    );
    // array_filter returns only elements with TRUE values
    foreach ($entities as $id => $value) {
      $form['ids'][$id] = array(
        '#type' => 'hidden',
        '#value' => $id,
        '#prefix' => '<li>',
        '#suffix' => check_plain(entity_label($this->entityType, $value)) . "</li>\n",
      );
    }
    $form['operation'] = array(
      '#type' => 'hidden',
      '#value' => 'delete',
    );
    $form['#submit'][] = 'entityadminui_bulk_operations_apply';
    $confirm_question = format_plural(count($entities), t('Are you sure you want to %op this item?', array('%op' => $operation['label'])), t('Are you sure you want to %op these items?', array('%op' => $operation['label'])));
    $form_state['rebuild'] = TRUE;
    return confirm_form($form, $confirm_question, 'admin/content', t('This action cannot be undone.'), t('Continue'), t('Cancel'));
  }

  /**
   * Apply a bulk operation to a set of selected entities.
   */
  public function bulkOperationsApply($form, &$form_state) {
    $operations = $this->getOperations();
    if (isset($operations[$form_state['values']['operation']]) && function_exists($operations[$form_state['values']['operation']]['callback'])) {
      $ids = array_filter($form_state['values']['ids']);
      $function = $operations[$form_state['values']['operation']]['callback'];
      $function($form, $form_state, $ids);
    }
  }

  /**
   * Generates the render array for the overview table for arbitrary entities
   * matching the given conditions.
   *
   * @param $conditions
   *   An array of conditions as needed by entity_load().

   * @return array
   *   A renderable array.
   */
  public function overviewTable($conditions = array()) {
    $headers = $this->overviewTableHeaders($conditions, array());

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // Add all conditions to query.
    foreach ($conditions as $key => $value) {
      $query->propertyCondition($key, $value);
    }

    if ($this->overviewPagerLimit) {
      $query->pager($this->overviewPagerLimit);
    }

    $query->tableSort($headers);

    // Add filters if there are any set.
    $filter_data = isset($_SESSION[$this->entityType . '_overview_filter']) ? $_SESSION[$this->entityType . '_overview_filter'] : array();
    foreach ($filter_data as $filter) {
      list($key, $value) = $filter;
      $query->propertyCondition($key, $value);
    }

    $results = $query->execute();

    $ids = isset($results[$this->entityType]) ? array_keys($results[$this->entityType]) : array();
    $entities = $ids ? entity_load($this->entityType, $ids) : array();

    // These two headers are added after the EFQ query has been run since we
    // do not want them to affect that query.
    // Add a header for status.
    if (!empty($this->entityInfo['exportable'])) {
      $headers[] = array('data' => t('Status'));
    }
    // Add a header for the operations columns.
    $headers['operations'] = array('data' => t('Operations'));

    $rows = array();
    foreach ($entities as $entity) {
      $rows[$entity->{$this->entityInfo['entity keys']['id']}] = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity);
    }

    $render = array(
      '#type' => 'tableselect',
      '#header' => $headers,
      '#options' => $rows,
      '#empty' => t('None.'),
    );
    return $render;
  }

  /**
   * Generates the table headers array used by EntityFieldQuery::tableSort().
   */
  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $header = $additional_header;
    foreach (array_keys($this->overviewPropertyList()) as $property) {
      if (isset($this->propertyInfo[$property])) {
        $header[$property] = array(
          'data' => $this->propertyInfo[$property]['label'],
          'type' => 'property',
          'specifier' => isset($this->propertyInfo[$property]['schema field']) ? $this->propertyInfo[$property]['schema field'] : '',
        );
      }
    }

    return $header;
  }

  /**
   * Generates the row for the passed entity and may be overridden in order to
   * customize the rows.
   *
   * @param $additional_cols
   *   Additional columns to be added after the entity label column.
   */
  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $entity_uri = entity_uri($this->entityType, $entity);
    $wrapper = entity_metadata_wrapper($this->entityType, $entity);
    $row = array();

    foreach (array_keys($this->overviewPropertyList()) as $property) {
      $value = $wrapper->{$property}->value(array('sanitize' => TRUE));
      // Watch for dates so we can format them nicer.
      if ($wrapper->{$property}->type() == 'date') {
        $value = format_date($value);
      }

      $row[$property] = array('data' => array(
        '#theme' => 'entity_ui_overview_item',
        '#label' => $value,
        '#name' => !empty($this->entityInfo['exportable']) ? entity_id($this->entityType, $entity) : FALSE,
        '#entity_type' => $this->entityType),
      );
    }

    // Always make the first item in the list a link to the entity.
    $keys = array_keys($row);
    $row[array_shift($keys)]['data']['#url'] = $entity_uri ? $entity_uri : FALSE;

    // Add in any passed additional cols.
    foreach ($additional_cols as $col) {
      $row[] = $col;
    }

    // Add a row for the exportable status.
    if (!empty($this->entityInfo['exportable'])) {
      $row['exportable_status'] = array('data' => array(
        '#theme' => 'entity_status',
        '#status' => $entity->{$this->statusKey},
      ));
    }
    // In case this is a bundle, we add links to the field ui tabs.
    $field_ui = !empty($this->entityInfo['bundle of']) && entity_type_is_fieldable($this->entityInfo['bundle of']) && module_exists('field_ui');
    // For exportable entities we add an export link.
    $exportable = !empty($this->entityInfo['exportable']);
    // If i18n integration is enabled, add a link to the translate tab.
    $i18n = !empty($this->entityInfo['i18n controller class']);

    // Add operations depending on the status.
    $operations = array();

    if (entity_has_status($this->entityType, $entity, ENTITY_FIXED)) {
      $row['operations'] = array('data' => l(t('clone'), $this->path . '/manage/' . $id . '/clone'), 'colspan' => $this->operationCount());
    }
    else {
      $operations[] = array('title' => t('edit'), 'href' => $this->path . '/manage/' . $id);

      if ($field_ui) {
        $operations[] = array('title' => t('manage fields'), 'href' => $this->path . '/manage/' . $id . '/fields');
        $operations[] = array('title' => t('manage display'), 'href' => $this->path . '/manage/' . $id . '/display');
      }
      if ($i18n) {
        $operations[] = array('title' => t('translate'), 'href' => $this->path . '/manage/' . $id . '/translate');
      }
      if ($exportable) {
        $operations[] = array('title' => t('clone'), 'href' => $this->path . '/manage/' . $id . '/clone');
      }

      if (empty($this->entityInfo['exportable']) || !entity_has_status($this->entityType, $entity, ENTITY_IN_CODE)) {
        $operations[] = array('title' => t('delete'), 'href' => $this->path . '/manage/' . $id . '/delete', 'query' => drupal_get_destination());
      }
      elseif (entity_has_status($this->entityType, $entity, ENTITY_OVERRIDDEN)) {
        $operations[] = array('title' => t('revert'), 'href' => $this->path . '/manage/' . $id . '/revert', 'query' => drupal_get_destination());
      }
      else {
        $operations[] = '';
      }

      $row['operations'] = theme('links', array('links' => $operations, 'attributes' => array('class' => 'inline')));
    }
    if ($exportable) {
      $operations[] = array('title' => t('export'), 'href' => $this->path . '/manage/' . $id . '/export');
    }

    if (count($operations)) {
      $row['operations'] = theme('links', array('links' => $operations, 'attributes' => array('class' => 'inline')));
    }

    return $row;
  }
}
